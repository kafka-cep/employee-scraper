FROM golang:alpine AS builder

COPY . .

RUN go build -o build employee_scraper

FROM alpine

COPY --from=builder /go/build ./build

CMD ["./build"]
