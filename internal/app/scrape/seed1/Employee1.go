package seed1

import (
	"encoding/json"
	"gitlab.com/kafka-cep/employee-scraper/pkg/app/timerInterval"
	"gitlab.com/kafka-cep/employee-scraper/pkg/model"
	"time"
)

type Employee1 struct {
	startTime time.Time
	timer     *timerInterval.TimerInterval
	Event     *model.Event
}

func NewEmployee1() *Employee1 {
	ev := &model.Event{
		Type:      "employee",
		Timestamp: time.Now(),
		Employee: model.Employee{
			ID:   1,
			Zone: 0,
			Long: -2,
			Lat:  3,
		},
	}

	startTime := time.Now()

	return &Employee1{startTime, timerInterval.New(startTime), ev}
}

// TODO: Как тут баги искать?
// TODO: Возможно надо внедрить паттерн состояние
// TODO: Переделать в методы создание model.Event и model.Employee
// TODO: Создать интерфейс для model.Event, чтобы брать и обрабатывать данные
// TODO: Да, хардкод но так было веселее. Рулетка на попадание в коллизии стен и правильности маршрута.
func (emp *Employee1) Scrape() ([]byte, error) {
	switch {
	case time.Since(emp.startTime) < time.Second*2:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*8:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 0, 6.5, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
		/*emp.Event.Timestamp = time.Now()
		emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 0, 6.5, time.Second*5, time.Since(emp.startTime)-time.Second*2)
		*/
	case time.Since(emp.startTime) < time.Second*13:
		emp.Event.Employee.Zone = 1
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 2, 6.5, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*21:
		emp.Event.Employee.Zone = 2
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 4, 6, time.Second*8)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 4, 6, time.Second*8, time.Since(emp.startTime)-time.Second*13)
	case time.Since(emp.startTime) < time.Second*31:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 6, 8, time.Second*10)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 6, 8, time.Second*10, time.Since(emp.startTime)-time.Second*21)
	case time.Since(emp.startTime) < time.Second*39:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 5, 10, time.Second*8)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 5, 10, time.Second*8, time.Since(emp.startTime)-time.Second*31)
	case time.Since(emp.startTime) < time.Second*43:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 4, 11, time.Second*4)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 4, 11, time.Second*4, time.Since(emp.startTime)-time.Second*39)
	case time.Since(emp.startTime) < time.Second*47:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 3, 10, time.Second*4)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 3, 10, time.Second*4, time.Since(emp.startTime)-time.Second*43)
	case time.Since(emp.startTime) < time.Second*57:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 3, 12, time.Second*10)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 3, 12, time.Second*10, time.Since(emp.startTime)-time.Second*47)
	case time.Since(emp.startTime) < time.Second*67:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 1, 13, time.Second*10)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 1, 13, time.Second*10, time.Since(emp.startTime)-time.Second*57)
	case time.Since(emp.startTime) < time.Second*71:
		emp.Event.Timestamp = time.Now()

	case time.Since(emp.startTime) < time.Second*81:
		emp.Event.Employee.Zone = 4
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 1, 14, time.Second*10)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 1, 14, time.Second*10, time.Since(emp.startTime)-time.Second*71)
	case time.Since(emp.startTime) < time.Second*121:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*131:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 1, 13, time.Second*10)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 1, 13, time.Second*10, time.Since(emp.startTime)-time.Second*121)
	case time.Since(emp.startTime) < time.Second*161:
		emp.Event.Employee.Zone = 2
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 10, 11, time.Second*30)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 10, 11, time.Second*30, time.Since(emp.startTime)-time.Second*131)
	case time.Since(emp.startTime) < time.Second*191:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 11, 9, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 11, 9, time.Second*5, time.Since(emp.startTime)-time.Second*161)
	case time.Since(emp.startTime) < time.Second*196:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*211:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 8, 7, time.Second*15)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 8, 7, time.Second*15, time.Since(emp.startTime)-time.Second*196)
	case time.Since(emp.startTime) < time.Second*241:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*251:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 6, 6, time.Second*10)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 6, 6, time.Second*10, time.Since(emp.startTime)-time.Second*241)
	case time.Since(emp.startTime) < time.Second*301:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*311:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 4, 5, time.Second*10)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 4, 5, time.Second*10, time.Since(emp.startTime)-time.Second*301)
	case time.Since(emp.startTime) < time.Second*316:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 3, 4.4, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 3, 4.4, time.Second*5, time.Since(emp.startTime)-time.Second*311)

	case time.Since(emp.startTime) < time.Second*321:
		emp.Event.Employee.Zone = 12
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 1, 3.5, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 1, 3.5, time.Second*5, time.Since(emp.startTime)-time.Second*316)
	case time.Since(emp.startTime) < time.Second*361:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*366:
		emp.Event.Employee.Zone = 12
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 3, 4.4, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 3, 4.4, time.Second*5, time.Since(emp.startTime)-time.Second*361)

	case time.Since(emp.startTime) < time.Second*371:
		emp.Event.Employee.Zone = 2
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 4, 6, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 4, 6, time.Second*5, time.Since(emp.startTime)-time.Second*366)
	case time.Since(emp.startTime) < time.Second*379:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 2, 6.5, time.Second*8)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 2, 6.5, time.Second*8, time.Since(emp.startTime)-time.Second*371)

	case time.Since(emp.startTime) < time.Second*384:
		emp.Event.Employee.Zone = 1
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 0, 6.5, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 0, 6.5, time.Second*5, time.Since(emp.startTime)-time.Second*379)

	case time.Since(emp.startTime) < time.Second*394:
		emp.Event.Employee.Zone = 0
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, -2, 10, time.Second*10)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

		//emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, -2, 10, time.Second*10, time.Since(emp.startTime)-time.Second*384)
	case time.Since(emp.startTime) < time.Second*400:
		emp.Event.Timestamp = time.Now()
	default:
		emp.Reset()
	}

	return json.Marshal(emp.Event)
}

func (emp *Employee1) Reset() {
	tmp := NewEmployee1()
	emp.startTime = tmp.startTime
	emp.timer = tmp.timer
	emp.Event = tmp.Event
}
