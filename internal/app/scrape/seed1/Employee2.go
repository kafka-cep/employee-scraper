package seed1

import (
	"encoding/json"
	"gitlab.com/kafka-cep/employee-scraper/pkg/app/timerInterval"
	"gitlab.com/kafka-cep/employee-scraper/pkg/model"
	"time"
)

type Employee2 struct {
	startTime time.Time
	timer     *timerInterval.TimerInterval
	Event     *model.Event
}

func NewEmployee2() *Employee2 {
	ev := &model.Event{
		Type:      "employee",
		Timestamp: time.Now(),
		Employee: model.Employee{
			ID:   2,
			Zone: 0,
			Long: -3,
			Lat:  6.5,
		},
	}

	startTime := time.Now()

	return &Employee2{startTime, timerInterval.New(startTime), ev}
}

// TODO: Возможно надо внедрить паттерн состояние
// TODO: Переделать в методы создание model.Event и model.Employee
// TODO: Создать интерфейс для model.Event, чтобы брать и обрабатывать данные
// TODO: Да, хардкод но так было веселее. Рулетка на попадание в коллизии стен и правильности маршрута.
// TODO: Как тут баги искать?
func (emp *Employee2) Scrape() ([]byte, error) {
	switch {
	case time.Since(emp.startTime) < time.Second*2:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*13:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 0, 6.5, time.Second*10)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
		/*emp.Event.Timestamp = time.Now()
		emp.Event.Employee.Long, emp.Event.Employee.Lat = Step(emp.Event.Employee.Long, emp.Event.Employee.Lat, 0, 6.5, time.Second*5, time.Since(emp.startTime)-time.Second*2)
		*/
	case time.Since(emp.startTime) < time.Second*18:
		emp.Event.Employee.Zone = 1
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 2, 7.5, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*21:
		emp.Event.Timestamp = time.Now()

	case time.Since(emp.startTime) < time.Second*51:
		emp.Event.Employee.Zone = 2
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 5, 7, time.Second*30)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*71:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 9, 9, time.Second*20)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*81:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*91:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 12, 9, time.Second*10)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*96:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 13.5, 10, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

	case time.Since(emp.startTime) < time.Second*101:
		emp.Event.Employee.Zone = 6
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 14.25, 11.75, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*141:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*146:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 12.5, 12.5, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*166:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*186:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 13.5, 10, time.Second*20)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

	case time.Since(emp.startTime) < time.Second*201:
		emp.Event.Employee.Zone = 2
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 12, 6, time.Second*15)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*203:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 13, 6, time.Second*2)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*207:
		emp.Event.Timestamp = time.Now()

	case time.Since(emp.startTime) < time.Second*210:
		emp.Event.Employee.Zone = 7
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 14, 6, time.Second*3)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*270:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*273:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 13, 6, time.Second*3)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

	case time.Since(emp.startTime) < time.Second*275:
		emp.Event.Employee.Zone = 2
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 12, 6, time.Second*2)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*315:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 6.5, 5, time.Second*40)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*325:
		emp.Event.Timestamp = time.Now()

	case time.Since(emp.startTime) < time.Second*330:
		emp.Event.Employee.Zone = 10
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 7, 4, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*335:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*340:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 8, 4, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*370:
		emp.Event.Timestamp = time.Now()

	case time.Since(emp.startTime) < time.Second*375:
		emp.Event.Employee.Zone = 8
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 10, 4.5, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*400:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*402:
		emp.Event.Employee.Zone = 8
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 10, 3.5, time.Second*2)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	case time.Since(emp.startTime) < time.Second*405:
		emp.Event.Timestamp = time.Now()
	case time.Since(emp.startTime) < time.Second*410:
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 8, 4, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

	case time.Since(emp.startTime) < time.Second*415:
		emp.Event.Employee.Zone = 10
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 6.5, 5, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

	case time.Since(emp.startTime) < time.Second*425:
		emp.Event.Employee.Zone = 2
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 2, 5.5, time.Second*10)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

	case time.Since(emp.startTime) < time.Second*427:
		emp.Event.Employee.Zone = 1
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, 0, 6.5, time.Second*2)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())

	case time.Since(emp.startTime) < time.Second*432:
		emp.Event.Employee.Zone = 0
		emp.Event.Timestamp = time.Now()
		emp.timer.Init(emp.Event.Employee.Long, emp.Event.Employee.Lat, -2, 10, time.Second*5)
		emp.Event.Employee.SetCoords(emp.timer.GetCoords())
	default:
		emp.Reset()
	}

	return json.Marshal(emp.Event)
}

func (emp *Employee2) Reset() {
	tmp := NewEmployee1()
	emp.startTime = tmp.startTime
	emp.timer = tmp.timer
	emp.Event = tmp.Event
}
