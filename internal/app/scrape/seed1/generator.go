package seed1

import (
	"gitlab.com/kafka-cep/employee-scraper/pkg/app/scrape"
	"go.uber.org/zap"
)

func Generator(logger *zap.Logger) ([]scrape.Scraper, error) {
	return []scrape.Scraper{NewEmployee1(), NewEmployee2()}, nil
}
