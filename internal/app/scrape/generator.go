package scrape

import (
	"gitlab.com/kafka-cep/employee-scraper/internal/app/scrape/seed1"
	"gitlab.com/kafka-cep/employee-scraper/pkg/app/scrape"
	"gitlab.com/kafka-cep/employee-scraper/pkg/config"
	"go.uber.org/zap"
)

func SeedGenerator(logger *zap.Logger, conf *config.Config) ([]scrape.Scraper, error) {
	switch conf.Gen {
	default:
		return seed1.Generator(logger)
	}
}
