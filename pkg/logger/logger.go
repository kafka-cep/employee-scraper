package logger

import (
	"gitlab.com/kafka-cep/employee-scraper/pkg/config"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"strings"
)

func NewLogger(cfg *config.LoggerConfig) *zap.Logger {
	conf := loggerConfig(cfg)
	return zap.Must(conf.Build())
}

func loggerConfig(cfg *config.LoggerConfig) zap.Config {
	return zap.Config{
		Level:             level(cfg),
		Development:       false,
		Encoding:          format(cfg),
		DisableCaller:     false,
		DisableStacktrace: false,
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:       "massage",
			LevelKey:         "level",
			TimeKey:          "timestamp",
			NameKey:          "service",
			StacktraceKey:    "stacktrace",
			EncodeTime:       zapcore.RFC3339TimeEncoder,
			EncodeLevel:      zapcore.CapitalLevelEncoder,
			EncodeName:       zapcore.FullNameEncoder,
			ConsoleSeparator: "\t",
		}, OutputPaths: []string{
			"stdout",
		},
		ErrorOutputPaths: []string{
			"stderr",
		},
		InitialFields: map[string]interface{}{
			"pid": os.Getpid(),
		},
	}
}

func level(cfg *config.LoggerConfig) zap.AtomicLevel {
	switch strings.ToLower(cfg.Level) {
	case "error":
		return zap.NewAtomicLevelAt(zap.ErrorLevel)
	case "warn":
		return zap.NewAtomicLevelAt(zap.WarnLevel)
	case "debug":
		return zap.NewAtomicLevelAt(zap.DebugLevel)
	default:
		return zap.NewAtomicLevelAt(zap.InfoLevel)
	}
}

func format(cfg *config.LoggerConfig) string {
	switch strings.ToLower(cfg.Format) {
	case "json":
		return "json"
	default:
		return "console"
	}
}
