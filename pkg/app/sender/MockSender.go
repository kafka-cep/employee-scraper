package sender

import (
	"fmt"
	"gitlab.com/kafka-cep/employee-scraper/pkg/config"
	"go.uber.org/zap"
)

type MockSender struct {
	logger *zap.Logger
}

func NewMockSender(logger *zap.Logger, conf *config.Config) (*MockSender, error) {
	res := &MockSender{
		logger: logger,
	}

	return res, nil
}

func (ag *MockSender) Send(data []byte) {
	fmt.Println(string(data))
}
