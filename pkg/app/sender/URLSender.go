package sender

import (
	"bytes"
	"fmt"
	"gitlab.com/kafka-cep/employee-scraper/pkg/config"
	"go.uber.org/zap"
	"io"
	"net/http"
	"net/url"
)

type URLSender struct {
	logger *zap.Logger

	url *url.URL
}

func NewURLSender(logger *zap.Logger, conf *config.Config) (*URLSender, error) {
	res := &URLSender{
		logger: logger,
		url:    nil,
	}

	_url, err := url.Parse(conf.URL)
	if err != nil {
		return nil, err
	}

	res.url = _url

	return res, nil
}

// TODO: Пределать в нормальный вид
// TODO: Добавить конкретности логирования
func (ag *URLSender) Send(data []byte) {
	resp, err := http.Post(ag.url.String(), "application/json", bytes.NewReader(data))
	if err != nil || resp == nil {
		ag.logger.Error(fmt.Sprintf("Send request error: %v", err),
			zap.Error(err),
		)
		return
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		ag.logger.Error(fmt.Sprintf("Cannot read response body from aggregator: %v", err))
		return
	}

	if resp.StatusCode >= 300 && resp.StatusCode < 200 {
		ag.logger.Error(fmt.Sprintf("Query not succsess: status code %v, body: %v", resp.StatusCode, string(body)))
		return
	}

	ag.logger.Info(fmt.Sprintf("Aggregate return response: %v", body))
}
