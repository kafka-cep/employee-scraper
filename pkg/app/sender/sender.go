package sender

import (
	"gitlab.com/kafka-cep/employee-scraper/pkg/config"
	"go.uber.org/zap"
)

type Sender interface {
	Send([]byte)
}

func SenderFactory(name string, logger *zap.Logger, config *config.Config) (Sender, error) {
	switch name {
	case "mock":
		return NewMockSender(logger, config)
	default:
		return NewURLSender(logger, config)
	}
}
