package scrape

import (
	"gitlab.com/kafka-cep/employee-scraper/pkg/config"
	"go.uber.org/zap"
)

type Scraper interface {
	Scrape() ([]byte, error)
}

type ScrapersCreator func(logger *zap.Logger, conf *config.Config) ([]Scraper, error)
