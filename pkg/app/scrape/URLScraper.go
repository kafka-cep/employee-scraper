package scrape

import (
	"bufio"
	"fmt"
	"gitlab.com/kafka-cep/employee-scraper/pkg/config"
	"go.uber.org/zap"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
)

// TODO:Провалидировать хост на корректность из файла
func URLScraperCreator(logger *zap.Logger, conf *config.Config) ([]Scraper, error) {
	var res []Scraper

	fl, err := os.Open(conf.IPsFile)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := fl.Close(); err != nil {
			logger.Warn(fmt.Sprintf("Close file error: %v", err))
		}
	}()

	scanner := bufio.NewScanner(fl)
	for scanner.Scan() {

		build := strings.Builder{}
		build.WriteString("http://")
		build.WriteString(scanner.Text())
		build.WriteString(conf.API)

		url2, err := url.Parse(build.String())
		if err != nil {
			return nil, err
		}
		res = append(res, NewURLScraper(logger, url2))
	}
	if err = scanner.Err(); err != nil {
		return nil, err
	}

	return res, nil
}

type URLScraper struct {
	*zap.Logger

	URL *url.URL
}

func NewURLScraper(logger *zap.Logger, url2 *url.URL) *URLScraper {
	res := &URLScraper{
		Logger: logger,
		URL:    url2,
	}

	return res
}

func (sc *URLScraper) Scrape() ([]byte, error) {
	resp, err := http.Get(sc.URL.String())
	if err != nil {
		sc.Error(fmt.Sprintf("Get request error: %v", err))
		return nil, err
	}
	defer func() {
		if err = resp.Body.Close(); err != nil {
			sc.Warn(fmt.Sprintf("Close body error: %v", err))
		}
	}()

	//TODO: Возможно вынести проверку запроса в отдельный метод
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		err = fmt.Errorf("Cannot get data from sensor %v. Response status code: %v", sc.URL.String(), resp.StatusCode)
		sc.Error(err.Error())
		return nil, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		err = fmt.Errorf("Cannot read data from sensor %v", sc.URL.String())
		sc.Error(err.Error())
		return nil, err
	}

	return body, nil

}
