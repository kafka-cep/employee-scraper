package timerInterval

import (
	"fmt"
	"math"
	"testing"
	"time"
)

func someRound(val float64, deep int) float64 {
	ratio := math.Pow(10, float64(deep))
	return math.Round(val*ratio) / ratio
}

func Test_SecondsSteps(t *testing.T) {
	timer := New(time.Now())
	for i := 0; i < 11; i++ {
		timer.Init(0, 0, 5, 5, time.Second*5)
		long, lat := timer.GetCoords()
		fmt.Printf("%.2f-%.2f\n", long, lat)
		if fmt.Sprintf("%.2f-%.2f", long, lat) != fmt.Sprintf("%.2f-%.2f", 0.5*float64(i), 0.5*float64(i)) {
			t.Error("Not equal func and expected")
		}
		time.Sleep(time.Millisecond * 500)
	}
}
