package model

type Employee struct {
	ID   int
	Zone int
	Long float64
	Lat  float64
}

func (emp *Employee) SetCoords(long, lat float64) {
	emp.Long = long
	emp.Lat = lat
}

func (emp *Employee) Coords() (float64, float64) {
	return emp.Long, emp.Lat
}
