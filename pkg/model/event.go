package model

import "time"

type Event struct {
	Type      string
	Timestamp time.Time
	Employee  Employee
}
