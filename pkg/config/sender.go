package config

import "os"

type SenderConfig struct {
	URL string `yaml:"sender_url" validate:"required,url"`
}

func (sc *SenderConfig) ENVS() {
	if val, ok := os.LookupEnv("SENDER_URL"); ok {
		sc.URL = val
	}
}
