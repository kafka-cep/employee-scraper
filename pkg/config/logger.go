package config

import "os"

type LoggerConfig struct {
	Level  string `yaml:"log_level" validate:"required,oneof=debug info warn error"`
	Format string `yaml:"log_format" validate:"required,oneof=json console"`
}

func (lc *LoggerConfig) SetDefaults() {
	lc.Level = "info"
	lc.Format = "json"
}

func (lc *LoggerConfig) ENVS() {
	if val, ok := os.LookupEnv("LOG_LEVEL"); ok {
		lc.Level = val
	}

	if val, ok := os.LookupEnv("LOG_FORMAT"); ok {
		lc.Format = val
	}
}
