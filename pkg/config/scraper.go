package config

import "os"

// TODO: Надо проверку на time.Duration в поле ScrapeInterval
type ScrapeConfig struct {
	IPsFile        string `yaml:"scrape_ips_file" validate:"required,file"`
	API            string `yaml:"scrape_api" validate:"required,filepath"`
	ScrapeInterval string `yaml:"scrape_interval" validate:"required"`
	Gen            string `yaml:"scrape_gen" validate:"required,numeric"`
}

func (sc *ScrapeConfig) ENVS() {
	if val, ok := os.LookupEnv("SCRAPE_IPS_FILE"); ok {
		sc.IPsFile = val
	}

	if val, ok := os.LookupEnv("SCRAPE_API"); ok {
		sc.IPsFile = val
	}

	if val, ok := os.LookupEnv("SCRAPE_INTERVAL"); ok {
		sc.ScrapeInterval = val
	}

	if val, ok := os.LookupEnv("SCRAPE_GEN"); ok {
		sc.IPsFile = val
	}
}
