package config

import (
	"errors"
	"fmt"
	"github.com/go-playground/validator/v10"
	"gopkg.in/yaml.v3"
	"os"
	"strings"
	"time"
)

type Config struct {
	*LoggerConfig `yaml:"-"`
	*ScrapeConfig `yaml:"-"`
	*SenderConfig `yaml:"-"`
}

// TODO: Покрыть логами полученные конфиги
// TODO: Убрать костыль из Validation в time.ParseDuration(cfg.ScrapeInterval)
func (cfg *Config) Validation() error {
	if err := validator.New().Struct(cfg); err != nil {
		return err
	}

	if _, err := time.ParseDuration(cfg.ScrapeInterval); err != nil {
		return err
	}

	return nil
}

func (cfg *Config) UnmarshalYAML(nd *yaml.Node) error {
	if err := nd.Decode(cfg.LoggerConfig); err != nil {
		return err
	}
	if err := nd.Decode(cfg.SenderConfig); err != nil {
		return err
	}
	if err := nd.Decode(cfg.ScrapeConfig); err != nil {
		return err
	}
	return nil
}

func CreateConfig() *Config {
	cfg := &Config{
		&LoggerConfig{},
		&ScrapeConfig{},
		&SenderConfig{},
	}

	cfg.LoggerConfig.SetDefaults()

	return cfg
}

func (cfg *Config) SetFromFile() error {
	if val, ok := os.LookupEnv("CONFIG_PATH"); ok {
		file, err := os.Open(val)
		if err != nil {
			return err
		}
		defer file.Close()

		dec := yaml.NewDecoder(file)
		return dec.Decode(cfg)
	}

	return errors.New("not set env CONFIG_PATH")
}

func (cfg *Config) SetFromEnvs() error {
	cfg.ScrapeConfig.ENVS()
	cfg.LoggerConfig.ENVS()
	cfg.SenderConfig.ENVS()

	return nil
}

func (cfg *Config) String() string {
	build := strings.Builder{}
	build.WriteString(fmt.Sprintln(cfg.ScrapeConfig))
	build.WriteString(fmt.Sprintln(cfg.LoggerConfig))
	build.WriteString(fmt.Sprintln(cfg.SenderConfig))
	return build.String()
}
