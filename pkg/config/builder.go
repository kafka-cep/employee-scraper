package config

import (
	"fmt"
	"log/slog"
)

type Builder interface {
	SetFromFile() error
	SetFromEnvs() error
}

// TODO: Избавиться от slog
func BuildConfig(cfg Builder) {
	if err := cfg.SetFromFile(); err != nil {
		slog.Error(fmt.Sprintf("Set config from file error: %v", err))
	}

	if err := cfg.SetFromEnvs(); err != nil {
		slog.Error(fmt.Sprintf("Set config from ENVS error: %v", err))
	}
}
