package main

import (
	"context"
	intScrape "gitlab.com/kafka-cep/employee-scraper/internal/app/scrape"
	"gitlab.com/kafka-cep/employee-scraper/pkg/app/scrape"
	"gitlab.com/kafka-cep/employee-scraper/pkg/app/sender"
	"gitlab.com/kafka-cep/employee-scraper/pkg/config"
	"gitlab.com/kafka-cep/employee-scraper/pkg/logger"
	"os"
	"os/signal"
	"time"
)

func main() {
	conf := config.CreateConfig()
	config.BuildConfig(conf)
	if err := conf.Validation(); err != nil {
		panic(err)
	}

	log := logger.NewLogger(conf.LoggerConfig)

	exChan := make(chan []byte, 10)

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()

	var scrapeService *scrape.Service
	var err error
	if conf.Gen == "0" {
		scrapeService, err = scrape.NewServiceScrapers(log, conf, scrape.URLScraperCreator)
	} else {
		scrapeService, err = scrape.NewServiceScrapers(log, conf, intScrape.SeedGenerator)
	}

	if err != nil {
		log.Fatal("Cannot create scrape service")
	}
	scrapeService.Run(ctx, exChan, 10)

	senderService, err := sender.NewSenderService(log, conf, "mock")
	if err != nil {
		log.Fatal("Cannot create sender service")
	}
	senderService.Run(ctx, 10, exChan)

	<-ctx.Done()

	time.Sleep(5 * time.Second)
}
